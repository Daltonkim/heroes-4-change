<a id="qodef-mobile-header-opener" href="#">
    <svg class="qodef-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="38px" height="13px" viewBox="0 0 38 13" xml:space="preserve">
        <g>
            <rect x="0" width="38" height="1"></rect>
            <rect x="0" width="38" height="1"></rect>
        </g>
        <g>
            <rect x="7" y="6" width="31" height="1"></rect>
            <rect x="7" y="6" width="31" height="1"></rect>
        </g>
        <g>
            <rect x="18" y="12" width="20" height="1"></rect>
            <rect x="18" y="12" width="20" height="1"></rect>
        </g>
    </svg>
</a>