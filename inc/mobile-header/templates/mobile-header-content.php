<?php

// Include mobile logo
topscorer_template_part( 'mobile-header', 'templates/parts/mobile-logo' );

// Include mobile navigation opener
topscorer_template_part( 'mobile-header', 'templates/parts/mobile-navigation-opener' );

// Include mobile navigation
topscorer_template_part( 'mobile-header', 'templates/parts/mobile-navigation' );