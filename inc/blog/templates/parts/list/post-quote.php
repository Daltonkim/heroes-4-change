<article <?php post_class( 'qodef-blog-item qodef-e' ); ?>>
	<div class="qodef-e-inner">
		<?php
		// Include post format part
		topscorer_template_part( 'blog', 'templates/parts/post-format/quote' ); ?>
	</div>
</article>