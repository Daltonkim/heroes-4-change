<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'TopscorerWelcomePage' ) ) {
	class TopscorerWelcomePage {

		/**
		 * Singleton class
		 */
		private static $instance;

		/**
		 * Get the instance of TopscorerWelcomePage
		 *
		 * @return self
		 */
        public static function get_instance() {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

		/**
		 * Cloning disabled
		 */
		private function __clone() {
		}

		/**
		 * Constructor
		 */
		private function __construct() {

			// Theme activation hook
			add_action( 'after_switch_theme', array( $this, 'init_activation_hook' ) );

			// Welcome page redirect on theme activation
			add_action( 'admin_init', array( $this, 'welcome_page_redirect' ) );

			// Add welcome page into theme options
			add_action( 'admin_menu', array( $this, 'add_welcome_page' ), 12 );

			//Enqueue theme welcome page scripts
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		}

		/**
		 * Init hooks on theme activation
		 */
		function init_activation_hook() {

			if ( ! is_network_admin() ) {
				set_transient( '_topscorer_welcome_page_redirect', 1, 30 );
			}
		}

		/**
		 * Redirect to welcome page on theme activation
		 */
		function welcome_page_redirect() {

			// If no activation redirect, bail
			if ( ! get_transient( '_topscorer_welcome_page_redirect' ) ) {
				return;
			}

			// Delete the redirect transient
			delete_transient( '_topscorer_welcome_page_redirect' );

			// If activating from network, or bulk, bail
			if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
				return;
			}

			// Redirect to welcome page
			wp_safe_redirect( add_query_arg( array( 'page' => 'topscorer_welcome_page' ), esc_url( admin_url( 'themes.php' ) ) ) );
			exit;
		}

		/**
		 * Add welcome page
		 */
		function add_welcome_page() {

			add_theme_page(
				esc_html__( 'About', 'topscorer' ),
				esc_html__( 'About', 'topscorer' ),
				current_user_can( 'edit_theme_options' ),
				'topscorer_welcome_page',
				array( $this, 'welcome_page_content' )
			);

			remove_submenu_page( 'themes.php', 'topscorer_welcome_page' );
		}

		/**
		 * Print welcome page content
		 */
		function welcome_page_content() {
			$params = array();

			$theme                       = wp_get_theme();
			$params['theme']             = $theme;
			$params['theme_name']        = esc_html( $theme->get( 'Name' ) );
			$params['theme_description'] = esc_html( $theme->get( 'Description' ) );
			$params['theme_version']     = $theme->get( 'Version' );
			$params['theme_screenshot']  = file_exists( TOPSCORER_ROOT_DIR . '/screenshot.png' ) ? TOPSCORER_ROOT . '/screenshot.png' : TOPSCORER_ROOT . '/screenshot.jpg';

			topscorer_template_part( 'welcome', 'templates/welcome', '', $params );
		}

		/**
		 * Enqueue theme welcome page scripts
		 */
		function enqueue_styles( $hook ) {

			if ( $hook === 'appearance_page_topscorer_welcome_page' ) {
				wp_enqueue_style( 'topscorer-welcome-page-style', TOPSCORER_INC_ROOT . '/welcome/assets/admin/css/welcome.min.css' );
			}
		}
	}
}

TopscorerWelcomePage::get_instance();