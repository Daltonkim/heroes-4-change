<?php

define( 'TOPSCORER_ROOT', get_template_directory_uri() );
define( 'TOPSCORER_ROOT_DIR', get_template_directory() );
define( 'TOPSCORER_ASSETS_ROOT', TOPSCORER_ROOT . '/assets' );
define( 'TOPSCORER_ASSETS_ROOT_DIR', TOPSCORER_ROOT_DIR . '/assets' );
define( 'TOPSCORER_ASSETS_CSS_ROOT', TOPSCORER_ASSETS_ROOT . '/css' );
define( 'TOPSCORER_ASSETS_CSS_ROOT_DIR', TOPSCORER_ASSETS_ROOT_DIR . '/css' );
define( 'TOPSCORER_ASSETS_JS_ROOT', TOPSCORER_ASSETS_ROOT . '/js' );
define( 'TOPSCORER_ASSETS_JS_ROOT_DIR', TOPSCORER_ASSETS_ROOT_DIR . '/js' );
define( 'TOPSCORER_INC_ROOT', TOPSCORER_ROOT . '/inc' );
define( 'TOPSCORER_INC_ROOT_DIR', TOPSCORER_ROOT_DIR . '/inc' );
